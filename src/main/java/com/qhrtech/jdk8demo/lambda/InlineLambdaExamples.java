/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.lambda;

import com.qhrtech.jdk8demo.util.DemoUtil;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class InlineLambdaExamples {

    public static void main( String... args ) {

        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit( () -> System.out.println( "This is an inline lambda of a runnable" ) );

        Runnable r = () -> System.out
                .println( "This is a runnable that has been declared but constructed via a lambda." );
        executorService.submit( r );

        Callable<Long> processor = System::currentTimeMillis;
        Future<Long> callableRuntime = executorService.submit( processor );

        Future<Long> futureLong = executorService.submit( () -> {
            final long l = 3L;
            System.out.println( l );
            return 6L;
        } );

        DemoUtil.closeAndWaitForExecutorService( executorService );

        try {
            System.out.println( "Value of callableRuntime: " + callableRuntime.get() );
            System.out.println( "Value of futureLong: " + futureLong.get() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }

        System.exit( 0 );
    }
}
