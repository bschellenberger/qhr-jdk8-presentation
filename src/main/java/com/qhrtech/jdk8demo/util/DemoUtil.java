/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class DemoUtil {

    public static void closeAndWaitForExecutorService( ExecutorService executorService ) {
        executorService.shutdown();
        try {
            if ( !executorService.awaitTermination( 60, TimeUnit.SECONDS ) ) {
                executorService.shutdownNow();
            }
        } catch ( InterruptedException ex ) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
