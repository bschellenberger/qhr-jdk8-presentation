/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.functional;

import com.qhrtech.jdk8demo.functional.impl.KosherVeganFilter;
import com.qhrtech.jdk8demo.functional.impl.ReverseString;
import com.qhrtech.jdk8demo.functional.impl.ScrambleString;
import com.qhrtech.jdk8demo.stream.entity.Food;
import com.qhrtech.jdk8demo.stream.factory.FoodFactory;
import com.qhrtech.jdk8demo.util.DemoUtil;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FunctionalTesting {

    public static void main( String... args ) {

        String[] transformStrings = new String[] { "onomatopoeia", "approbation", "iconoclast", "heterogeneous", "subjugate" };

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        IntStream.range( 0, 100 ).forEach( i -> executorService.submit( () -> System.out.println( "Number " + i ) ) );

        Collection<String> foodIWant = new ArrayList<>();
        foodIWant.add( "Brisket" );
        foodIWant.add( "Popcorn" );

        FoodFunction<Food> compareCalories = ( s ) -> ( s.parallelStream()
                .sorted( Comparator.comparingInt( f -> f.getDietaryInformation().getCaloriesPerServing() ) )
                .collect( Collectors.toList() ) );

        Collection<Food> sortedByCaloriesPerServing = compareCalories.process( FoodFactory.getFood() );
        sortedByCaloriesPerServing.forEach( food -> System.out.println( displayNameAndCalories( food ) ) );

        System.out.println( "------Food for exact string match------" );
        Collection<Food> exactFoods = getFoodSearchFunction( false ).apply( foodIWant );
        exactFoods.forEach( food -> System.out.println( food.getName() ) );
        foodConsumer( exactFoods );
        System.out.println( "------Food for contains match------" );
        Collection<Food> containsFoods = getFoodSearchFunction( true ).apply( foodIWant );
        containsFoods.forEach( food -> System.out.println( food.getName() ) );
        foodConsumer( containsFoods );

        System.out.println( "------Kosher Vegan Filter------" );

        new KosherVeganFilter<>().process( ( FoodFactory.getFood() ) )
                .forEach( food -> System.out.println( food.getName() ) );

        StringFunc currentFunc = new ReverseString();

        System.out.println( "------Reversing Strings------" );
        runStringFunction( Arrays.stream( transformStrings ), currentFunc );

        currentFunc = new ScrambleString();
        System.out.println( "------Scrambling Strings------" );
        runStringFunction( Arrays.stream( transformStrings ), currentFunc );

        DemoUtil.closeAndWaitForExecutorService( executorService );

        System.exit( 0 );
    }

    private static String displayNameAndCalories( Food food ) {
        return food.getName() + " - Calories: " + food.getDietaryInformation().getCaloriesPerServing();
    }

    private static void foodConsumer( Collection<Food> food ) {
        System.out.println( food.stream().map( Food::getName )
                                    .collect( Collectors.joining( " and ", "I'm eating ", " for dinner! Yum yum!" ) ) );
    }

    private static void runStringFunction( Stream<String> strings, StringFunc func ) {
        strings.forEach( curString -> System.out.println( curString + " - " + func.transformString( curString ) ) );
    }

    /**
     * Returns a function that will attempt to find foods that match the passed in Strings; case insensitive.
     *
     * @return a function that returns Foods matching the provided Strings
     */
    private static Function<Collection<String>, Collection<Food>> getFoodSearchFunction( final boolean contains ) {
        return ( ( strings ) -> (Collection<Food>) FoodFactory.getFood().parallelStream().filter(
                f -> strings.parallelStream().anyMatch(
                        s -> contains ? StringUtils.containsIgnoreCase( f.getName(), s ) : s
                                .equalsIgnoreCase( f.getName() ) ) ).collect( Collectors.toList() ) );
    }
}
