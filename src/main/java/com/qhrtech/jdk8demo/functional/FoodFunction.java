/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.functional;

import com.qhrtech.jdk8demo.stream.entity.Food;
import java.util.Collection;

@FunctionalInterface
public interface FoodFunction<T extends Food> {

    Collection<T> process( Collection<T> foods );
}
