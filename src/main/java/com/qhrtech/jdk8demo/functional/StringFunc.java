/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.functional;

@FunctionalInterface
public interface StringFunc {

    String transformString( String string );
}
