/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.functional.impl;

import com.qhrtech.jdk8demo.functional.FoodFunction;
import com.qhrtech.jdk8demo.stream.entity.Food;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class KosherVeganFilter<T extends Food> implements FoodFunction<T> {

    @Override
    public Collection<T> process( Collection<T> foods ) {
        List<T> foodToReturn = new ArrayList<>();
        foods.parallelStream().forEach( currentFood -> {
            if ( currentFood.isKosher() && currentFood.isVegan() ) {
                foodToReturn.add( currentFood );
            }
        } );
        return foodToReturn;
    }
}
