/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.functional.impl;

import com.qhrtech.jdk8demo.functional.StringFunc;

public class ReverseString implements StringFunc {

    @Override
    public String transformString( String string ) {
        return new StringBuilder( string ).reverse().toString();
    }
}
