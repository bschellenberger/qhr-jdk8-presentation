/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.functional.impl;

import com.qhrtech.jdk8demo.functional.StringFunc;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ScrambleString implements StringFunc {

    @Override
    public String transformString( String string ) {
        List<Character> chars = string.chars().mapToObj( c -> (char) c ).collect( Collectors.toList() );
        Collections.shuffle( chars );
        return chars.stream().map( String::valueOf ).collect( Collectors.joining() );
    }

    public void MethodExclusiveToScrambleString() {

    }
}
