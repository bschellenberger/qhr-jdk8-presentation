/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity;

import org.apache.commons.lang3.RandomStringUtils;
import java.util.Random;

public class RandomFood extends Food {

    private static final Random random = new Random();
    private final boolean vegetarian = random.nextBoolean();
    private final boolean vegan = vegetarian && random.nextBoolean();
    private final boolean kosher = random.nextBoolean();
    private final boolean halal = random.nextBoolean();

    public RandomFood() {
        setName( RandomStringUtils.random( Math.max( random.nextInt( 51 ), 10 ), true, false ) );
        DietaryInformation.ServingUnit unit = random
                .nextBoolean() ? DietaryInformation.ServingUnit.OUNCES : DietaryInformation.ServingUnit.GRAMS;
        double totalFat = ( (double) random.nextInt( 500 ) ) / 10;
        double saturatedFat = random.nextInt( Math.max( 1, (int) ( totalFat * 10 ) / 10 ) );
        double transFat = random.nextInt( Math.max( 1, ( (int) ( ( totalFat * 10 ) - ( saturatedFat * 10 ) ) / 10 ) ) );
        setDietaryInformation( new DietaryInformation( Math.max( 60, random.nextInt( 301 ) ),
                                                       unit == DietaryInformation.ServingUnit.OUNCES ? Math
                                                               .max( 1, random.nextInt( 17 ) ) : Math
                                                               .max( 24, random.nextInt( 455 ) ), unit, totalFat,
                                                       saturatedFat, transFat, random.nextInt( 60 ),
                                                       random.nextInt( 30 ), random.nextInt( 12 ) ) );
    }

    @Override
    public boolean isVegan() {
        return vegan;
    }

    @Override
    public boolean isVegetarian() {
        return vegetarian;
    }

    @Override
    public boolean isKosher() {
        return kosher;
    }

    @Override
    public boolean isHalal() {
        return halal;
    }
}
