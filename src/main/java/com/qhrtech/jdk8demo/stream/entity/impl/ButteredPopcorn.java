/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;

public class ButteredPopcorn extends Popcorn {

    public ButteredPopcorn() {
        setName( "Buttered Popcorn" );
        setDietaryInformation(
                new DietaryInformation( 160, 28, DietaryInformation.ServingUnit.GRAMS, 12, 1.5, 0, 12, 2, 2 ) );
    }

    @Override
    public boolean isVegan() {
        return false;
    }
}
