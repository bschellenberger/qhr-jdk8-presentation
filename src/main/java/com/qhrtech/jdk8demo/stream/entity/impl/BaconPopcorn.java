/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;

public class BaconPopcorn extends Popcorn {

    public BaconPopcorn() {
        setName( "Bacon Popcorn" );
        setDietaryInformation(
                new DietaryInformation( 130, 28, DietaryInformation.ServingUnit.GRAMS, 8, 4, 0, 14, 2, 3 ) );
    }

    @Override
    public boolean isVegan() {
        return false;
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }

    @Override
    public boolean isKosher() {
        return false;
    }

    @Override
    public boolean isHalal() {
        return false;
    }
}
