/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public abstract class Food {

    private String name;
    private DietaryInformation dietaryInformation;

    Food() {

    }

    public abstract boolean isVegan();

    public abstract boolean isVegetarian();

    public abstract boolean isKosher();

    public abstract boolean isHalal();

    public String getName() {
        return name;
    }

    protected void setName( String name ) {
        this.name = name;
    }

    public DietaryInformation getDietaryInformation() {
        return dietaryInformation;
    }

    protected void setDietaryInformation( DietaryInformation dietaryInformation ) {
        this.dietaryInformation = dietaryInformation;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) {
            return true;
        }

        if ( !( o instanceof Food ) ) {
            return false;
        }

        Food food = (Food) o;

        return new EqualsBuilder().append( getDietaryInformation(), food.getDietaryInformation() )
                .append( getName(), food.getName() ).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder( 17, 37 ).append( getDietaryInformation() ).append( getName() ).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder( this ).append( "name", name ).append( "dietaryInformation", dietaryInformation )
                .toString();
    }
}
