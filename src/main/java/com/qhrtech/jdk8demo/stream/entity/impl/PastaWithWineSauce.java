/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;

public class PastaWithWineSauce extends Spaghetti {

    public PastaWithWineSauce() {
        this.setName( "Spaghetti w/ Wine Sauce" );
        this.setDietaryInformation(
                new DietaryInformation( 226, 131, DietaryInformation.ServingUnit.GRAMS, 14.8, 3.9, 0.2, 33.3, 6.4,
                                        2.1 ) );
    }

    @Override
    public boolean isVegan() {
        return true;
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }

    @Override
    public boolean isKosher() {
        return true;
    }

    @Override
    public final boolean isHalal() {
        return false;
    }
}
