/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;
import com.qhrtech.jdk8demo.stream.entity.Meat;

public class Brisket extends Meat {

    public Brisket() {
        setName( "Beef Brisket" );
        setDietaryInformation(
                new DietaryInformation( 300, 6, DietaryInformation.ServingUnit.OUNCES, 12, 0.5, 0, 0, 36, 0 )

        );
    }

    @Override
    public boolean isKosher() {
        return true;
    }

    @Override
    public boolean isHalal() {
        return true;
    }
}
