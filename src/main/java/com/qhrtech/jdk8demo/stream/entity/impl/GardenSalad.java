/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;
import com.qhrtech.jdk8demo.stream.entity.Plant;

public class GardenSalad extends Plant {

    public GardenSalad() {
        setName( "Garden Salad" );
        setDietaryInformation(
                new DietaryInformation( 161, 220, DietaryInformation.ServingUnit.GRAMS, 11, 1.7, 0, 15, 2.3, 2.7 ) );
    }

    @Override
    public boolean isVegan() {
        return true;
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }

    @Override
    public boolean isKosher() {
        return true;
    }

    @Override
    public boolean isHalal() {
        return true;
    }
}
