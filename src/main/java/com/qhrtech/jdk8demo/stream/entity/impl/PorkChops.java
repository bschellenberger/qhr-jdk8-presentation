/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;
import com.qhrtech.jdk8demo.stream.entity.Meat;

public class PorkChops extends Meat {

    public PorkChops() {
        setName( "Pork Chops" );
        setDietaryInformation(
                new DietaryInformation( 231, 100, DietaryInformation.ServingUnit.GRAMS, 14, 4.3, 0, 0, 24, 0 ) );
    }

    @Override
    public boolean isKosher() {
        return false;
    }

    @Override
    public boolean isHalal() {
        return false;
    }
}
