/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;
import com.qhrtech.jdk8demo.stream.entity.Plant;

public class Spaghetti extends Plant {

    public Spaghetti() {
        setName( "Spaghetti" );
        setDietaryInformation(
                new DietaryInformation( 158, 100, DietaryInformation.ServingUnit.GRAMS, 0.9, 0.2, 0, 31, 6, 1.8 ) );
    }

    @Override
    public boolean isVegan() {
        return true;
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }

    @Override
    public boolean isKosher() {
        return true;
    }

    @Override
    public boolean isHalal() {
        return true;
    }
}
