/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity.impl;

import com.qhrtech.jdk8demo.stream.entity.DietaryInformation;
import com.qhrtech.jdk8demo.stream.entity.Plant;

public class Popcorn extends Plant {

    public Popcorn() {
        setName( "Popcorn" );
        setDietaryInformation(
                new DietaryInformation( 106, 1, DietaryInformation.ServingUnit.OUNCES, 1.2, 0.2, 0, 21, 3.1, 3.6 ) );
    }

    @Override
    public boolean isVegan() {
        return true;
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }

    @Override
    public boolean isKosher() {
        return true;
    }

    @Override
    public boolean isHalal() {
        return true;
    }
}
