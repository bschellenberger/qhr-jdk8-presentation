/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity;

public abstract class Meat extends Food {

    /**
     * Copyright 2018 QHR Technologies
     */

    protected Meat() {

    }

    @Override
    final public boolean isVegan() {
        return false;
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }
}
