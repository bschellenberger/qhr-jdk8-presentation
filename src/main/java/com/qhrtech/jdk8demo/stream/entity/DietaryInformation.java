/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@SuppressWarnings( "WeakerAccess" )
public class DietaryInformation {

    private final int servingSize;
    private final ServingUnit servingUnit;
    private final int caloriesPerServing;
    private final double fat;
    private final double saturatedFat;
    private final double transFat;
    private final double carbohydrates;
    private final double protein;
    private final double fiber;

    public DietaryInformation( int caloriesPerServing, int servingSize, ServingUnit servingUnit, double fat, double saturatedFat, double transFat, double carbohydrates, double protein, double fiber ) {
        this.protein = protein;
        this.fiber = fiber;
        this.fat = fat;
        this.saturatedFat = saturatedFat;
        this.transFat = transFat;
        this.carbohydrates = carbohydrates;
        this.servingUnit = servingUnit;
        this.servingSize = servingSize;
        this.caloriesPerServing = caloriesPerServing;
    }

    public double getProtein() {
        return protein;
    }

    public double getFiber() {
        return fiber;
    }

    public double getFat() {
        return fat;
    }

    public ServingUnit getServingUnit() {
        return servingUnit;
    }

    public int getServingSize() {
        return servingSize;
    }

    public int getCaloriesPerServing() {
        return caloriesPerServing;
    }

    public double getSaturatedFat() {
        return saturatedFat;
    }

    public double getTransFat() {
        return transFat;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getTotalFat() {
        return fat + saturatedFat + transFat;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) {
            return true;
        }

        if ( !( o instanceof DietaryInformation ) ) {
            return false;
        }

        DietaryInformation that = (DietaryInformation) o;

        return new EqualsBuilder().append( getProtein(), that.getProtein() ).append( getFiber(), that.getFiber() )
                .append( getFat(), that.getFat() ).append( getSaturatedFat(), that.getSaturatedFat() )
                .append( getTransFat(), that.getTransFat() ).append( getCarbohydrates(), that.getCarbohydrates() )
                .append( getServingSize(), that.getServingSize() )
                .append( getCaloriesPerServing(), that.getCaloriesPerServing() )
                .append( getServingUnit(), that.getServingUnit() ).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder( 17, 37 ).append( getProtein() ).append( getFiber() ).append( getFat() )
                .append( getSaturatedFat() ).append( getTransFat() ).append( getCarbohydrates() )
                .append( getServingUnit() ).append( getServingSize() ).append( getCaloriesPerServing() ).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder( this ).append( "servingSize", servingSize ).append( "servingUnit", servingUnit )
                .append( "caloriesPerServing", caloriesPerServing ).append( "fat", fat )
                .append( "saturatedFat", saturatedFat ).append( "transFat", transFat )
                .append( "carbohydrates", carbohydrates ).append( "protein", protein ).append( "fiber", fiber )
                .toString();
    }

    public enum ServingUnit {
        OUNCES( "ounces", "oz" ), GRAMS( "grams", "g" );

        private String displayName;
        private String abbreviation;

        ServingUnit( String displayName, String abbreviation ) {
            this.displayName = displayName;
            this.abbreviation = abbreviation;
        }

    }
}

