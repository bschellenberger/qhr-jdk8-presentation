/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream;

import java.util.Arrays;
import java.util.Collection;

public class BasicStreamExamples {

    public static void main( String... args ) {
        BasicStreamExamples basicStreamExamples = new BasicStreamExamples();
        basicStreamExamples.run();
        System.exit( 0 );
    }

    @SuppressWarnings( { "SimplifyStreamApiCallChains", "StreamToLoop" } )
    private void run() {
        Collection<String> deliciousFoods = getDeliciousFoods();
        for ( String food : deliciousFoods ) {
            System.out.println( food + " is a deliciousFood" );
        }

        //Ordered
        deliciousFoods.stream().forEach( food -> System.out.println( food + " is a delicious food." ) );

        //Random, unordered
        deliciousFoods.parallelStream().forEach( food -> System.out.println( food + " is a delicious food." ) );
    }

    private Collection<String> getDeliciousFoods() {
        return Arrays
                .asList( "Apple", "Macaroni And Cheese", "Strawberry", "Ice Cream", "Baked Potato", "Steak", "Chicken",
                         "Eggs", "Butternut Squash" );
    }

    private void eatStream( String toEat ) {
        System.out.println( toEat + " is " + ( getDeliciousFoods().parallelStream()
                .anyMatch( deliciousFood -> deliciousFood.equals( toEat ) ) ? "" : " not " ) + " a delicious food." );
    }

    private void eatStreamAlternative( String toEat ) {
        boolean isDelicious = ( getDeliciousFoods().parallelStream()
                .anyMatch( deliciousFood -> deliciousFood.equals( toEat ) ) );

        System.out.print( toEat + " is " + ( isDelicious ? "" : " not " ) + " a delicious food." );
    }

    private void eatForLoop( String toEat ) {
        boolean isDelicious = false;
        for ( String deliciousFood : getDeliciousFoods() ) {
            if ( !deliciousFood.equals( toEat ) ) {
                continue;
            }
            isDelicious = true;
            break;
        }
        System.out.println( toEat + " is " + ( isDelicious ? "" : " not " ) + " a delicious food." );
    }
}
