/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream;

import com.qhrtech.jdk8demo.stream.entity.Food;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import static com.qhrtech.jdk8demo.stream.factory.FoodFactory.getFood;

public class AdvancedStreamExamples<CategorizeFoodFunction> {

    @SuppressWarnings( "Convert2Lambda" )
    private static final Comparator<Food> alphabeticalComparator = new Comparator<Food>() {
        @Override
        public int compare( Food o1, Food o2 ) {
            return StringUtils.compare( o1.getName(), o2.getName() );
        }
    };
    private Set<Food> veganFood = new HashSet<>();
    private Set<Food> halalFood = new HashSet<>();
    private Set<Food> kosherFood = new HashSet<>();
    private Set<Food> vegetarianFood = new HashSet<>();

    public static void main( String... args ) {
        System.out.println( "----------Begin call toString----------" );
        testStreamsVSCollectionsPerformance( getCallToStringFunction() );
        System.out.println( "----------End call toString----------" );

        AdvancedStreamExamples.CategorizeFoodFunction categorizeFoodFunction = new AdvancedStreamExamples.CategorizeFoodFunction();
        System.out.println( "----------Begin categorizing food----------" );
        testStreamsVSCollectionsPerformance( categorizeFoodFunction );
        categorizeFoodFunction.outputFoodGroups();
        System.out.println( "----------End categorizing food----------" );

        System.out.println( "----------Begin sorting food----------" );
        testAlphabeticalSort();
        System.out.println( "----------End sorting food----------" );

        System.exit( 0 );
    }

    @SuppressWarnings( "ResultOfMethodCallIgnored" )
    private static Function<Food, ?> getCallToStringFunction() {
        return (Function<Food, Void>) food -> {
            food.toString();
            return null;
        };
    }

    private static void testStreamsVSCollectionsPerformance( Function<Food, ?> function ) {
        testStreamsVSCollectionsPerformance( function, 1, true );
    }

    private static void testStreamsVSCollectionsPerformance( Function<Food, ?> function, long iterations ) {
        testStreamsVSCollectionsPerformance( function, iterations, true );
    }

    @SuppressWarnings( "SimplifyStreamApiCallChains" )
    private static void testStreamsVSCollectionsPerformance( Function<Food, ?> function, long iterations, boolean outputResults ) {
        long start, end, runtime;
        Collection<Food> food = getFood();
        Collection<Food> currentCollection = new ArrayList<>( food );
        runtime = 0;
        for ( long i = 0; i < iterations; i++ ) {
            start = System.currentTimeMillis();
            for ( Food f : currentCollection ) {
                function.apply( f );
            }
            end = System.currentTimeMillis();
            runtime += ( end - start );
        }

        if ( outputResults ) {
            System.out.println( "ArrayList For Loop took " + runtime + "ms to process " + iterations + " iterations" );
        }

        runtime = 0;
        currentCollection = new ArrayList<>( food );
        for ( long i = 0; i < iterations; i++ ) {
            start = System.currentTimeMillis();
            currentCollection.forEach( function::apply );
            end = System.currentTimeMillis();
            runtime += ( end - start );
        }

        if ( outputResults ) {
            System.out.println( "ArrayList ForEach took " + runtime + "ms to process " + iterations + " iterations" );
        }

        runtime = 0;
        currentCollection = new HashSet<>( food );
        for ( long i = 0; i < iterations; i++ ) {
            start = System.currentTimeMillis();
            for ( Food f : currentCollection ) {
                function.apply( f );
            }
            end = System.currentTimeMillis();
            runtime += ( end - start );
        }

        if ( outputResults ) {
            System.out.println( "HashSet For Loop took " + runtime + "ms to process " + iterations + " iterations" );
        }

        runtime = 0;
        currentCollection = new HashSet<>( food );
        for ( long i = 0; i < iterations; i++ ) {
            start = System.currentTimeMillis();
            currentCollection.forEach( function::apply );
            end = System.currentTimeMillis();
            runtime += ( end - start );
        }
        System.out.println( "HashSet ForEach took " + runtime + "ms to process " + iterations + " iterations" );
        runtime = 0;
        for ( long i = 0; i < iterations; i++ ) {
            start = System.currentTimeMillis();
            food.stream().forEach( function::apply );
            end = System.currentTimeMillis();
            runtime += ( end - start );
        }

        if ( outputResults ) {
            System.out.println( "Stream foreach took " + runtime + "ms to process " + iterations + " iterations" );
        }

        runtime = 0;
        for ( long i = 0; i < iterations; i++ ) {
            start = System.currentTimeMillis();
            food.parallelStream().forEach( function::apply );
            end = System.currentTimeMillis();
            runtime += ( end - start );
        }

        if ( outputResults ) {
            System.out.println(
                    "Parallel stream foreach took " + runtime + "ms to process " + iterations + " iterations" );
        }
    }

    @SuppressWarnings( "ResultOfMethodCallIgnored" )
    private static void testAlphabeticalSort() {
        long start, end;
        Collection<Food> food = getFood();
        List<Food> foodList = new ArrayList<>( food );

        start = System.currentTimeMillis();
        foodList.sort( alphabeticalComparator );
        end = System.currentTimeMillis();
        System.out.println( "List.sort took " + ( end - start ) + "ms to alphabetize foods." );

        start = System.currentTimeMillis();
        food.stream().sorted( alphabeticalComparator );
        end = System.currentTimeMillis();
        System.out.println( "stream().sorted() took " + ( end - start ) + "ms to alphabetize foods." );

        start = System.currentTimeMillis();
        food.parallelStream().sorted( alphabeticalComparator );
        end = System.currentTimeMillis();
        System.out.println( "parallelStream().sorted() took " + ( end - start ) + "ms to alphabetize foods." );
    }

    @SuppressWarnings( "WeakerAccess" )
    private static class NamedSet<T> {

        private final String name;
        private final Set<T> set;

        private NamedSet( String name, Class<T> clazz ) {
            this.name = name;
            this.set = new HashSet<>();
        }

        private NamedSet( String name, Set<T> set ) {
            this.name = name;
            this.set = set;
        }

        public String getName() {
            return name;
        }

        public Set<T> getSet() {
            return set;
        }

        public void add( T t ) {
            set.add( t );
        }

        public void remove( T t ) {
            set.remove( t );
        }
    }

    private static class CategorizeFoodFunction implements Function<Food, Void> {

        private static NamedSet<Food> veganFood = new NamedSet<>( "Vegan Food", Food.class );
        private static NamedSet<Food> halalFood = new NamedSet<>( "Halal Food", new HashSet<>() );
        private static NamedSet<Food> kosherFood = new NamedSet<>( "Kosher Food", Food.class );
        private static NamedSet<Food> vegetarianFood = new NamedSet<>( "Vegetarian Food", Food.class );

        private static Set<NamedSet<Food>> foodSets = new HashSet<>();
        static {
            foodSets.add( veganFood );
            foodSets.add( halalFood );
            foodSets.add( kosherFood );
            foodSets.add( vegetarianFood );
        }
        @Override
        public Void apply( Food food ) {
            if ( food.isVegan() ) {
                veganFood.add( food );
            }
            if ( food.isHalal() ) {
                halalFood.add( food );
            }
            if ( food.isKosher() ) {
                kosherFood.add( food );
            }
            if ( food.isVegetarian() ) {
                vegetarianFood.add( food );
            }
            return null;
        }

        private void outputFoodGroups() {
            foodSets.parallelStream().forEach( set -> System.out.println(
                    set.getName() + ": " + set.getSet().parallelStream().map( Food::getName )
                            .collect( Collectors.joining( ", " ) ) ) );
        }
    }
}
