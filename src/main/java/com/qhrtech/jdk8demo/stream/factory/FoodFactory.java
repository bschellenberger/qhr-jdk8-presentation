/*
 * Copyright (c) 2018 QHR Technologies
 */

package com.qhrtech.jdk8demo.stream.factory;

import com.qhrtech.jdk8demo.stream.entity.Food;
import com.qhrtech.jdk8demo.stream.entity.RandomFood;
import com.qhrtech.jdk8demo.stream.entity.impl.BaconPopcorn;
import com.qhrtech.jdk8demo.stream.entity.impl.Brisket;
import com.qhrtech.jdk8demo.stream.entity.impl.ButteredPopcorn;
import com.qhrtech.jdk8demo.stream.entity.impl.GardenSalad;
import com.qhrtech.jdk8demo.stream.entity.impl.Popcorn;
import com.qhrtech.jdk8demo.stream.entity.impl.PorkChops;
import com.qhrtech.jdk8demo.stream.entity.impl.Spaghetti;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.IntStream;

public class FoodFactory {

    private static final Collection<Food> ALL_FOOD = new HashSet<>();
    private static boolean RANDOM_FOOD = false;
    static {
        addAllFoods();
    }
    public static Collection<Food> getFood() {
        return new HashSet<>( ALL_FOOD );
    }

    private static void addAllFoods() {
        if ( RANDOM_FOOD ) {
            IntStream.range( 0, 1000 ).forEach( i -> addFood( new RandomFood() ) );
        } else {
            addFood( new Brisket() );
            addFood( new GardenSalad() );
            addFood( new PorkChops() );
            addFood( new Spaghetti() );
            addFood( new Popcorn() );
            addFood( new ButteredPopcorn() );
            addFood( new BaconPopcorn() );
        }
    }

    private static void addFood( Food food ) {
        ALL_FOOD.add( food );
    }
}
